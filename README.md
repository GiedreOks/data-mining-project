## Data mining project 

* This project was done in order to complete a university project assignment.
* The goal of the project was to predict customer churn for a mobile network operator using machine learning algorithms. 
* The project scripts are in Python and distributed computing was performed using using Spark Python API.

The commit list is not complete - the project was done in pairs and only my part is uploaded.

