import pyspark
from pyspark.ml.feature import VectorAssembler, StandardScaler
from pyspark.ml.pipeline import Pipeline
from pyspark.ml.clustering import KMeans
import jsonlines

import csv

spark = (
    pyspark.sql.SparkSession
        .builder
        .appName("Python Spark K-means minimal example")
        .enableHiveSupport()
        .getOrCreate()
)

usage_df = spark.read.csv("./data/aggregated_customer_usage.csv",
                          header=True, inferSchema=True)
with open("./data/columns_continuous__agg_usage.txt") as f:
    continuous_columns = f.read().rstrip().split(",")

vector_assembler = VectorAssembler(inputCols=continuous_columns,
                                   outputCol="features")
standard_scaler = StandardScaler(inputCol="features",
                                 outputCol="scaled_features",
                                 withStd=True, withMean=True)

featurization_pipeline = Pipeline(stages=[vector_assembler, standard_scaler])

featurization_pipeline_model = featurization_pipeline.fit(usage_df)
model_scaler = featurization_pipeline_model.stages[-1]
featurized_usage_df = featurization_pipeline_model.transform(usage_df)

err = open("./data/errors.txt", "w")
err = open("./data/errors.txt", "a")

# Number of clusters
qClusters = 4

kAndCost = []
for i in range(2, qClusters+1):
    kmeans = KMeans(featuresCol="scaled_features", k=i)
    kmeans_model = kmeans.fit(featurized_usage_df)
    cost = kmeans_model.computeCost(featurized_usage_df)
    err.writelines("SSE: " + str(cost) + " when k = " + str(i) + " ")
    kAndCost.append((i, cost))

# Saving SSE to CSV file for further metric calculation
with open('./data/kMeans.csv', "w", newline='') as f:
    writer = csv.writer(f)
    writer.writerow(["k", "SSE"])
    for i in range(2, qClusters+1):
        writer.writerow(kAndCost[i-2])


clustered_kmeans_df = kmeans_model.transform(featurized_usage_df)
clustered_kmeans_df.toPandas().to_csv("./data/cluster.csv",
                                      header=True, index=False)

centers = kmeans_model.clusterCenters()
with open('./data/cluster_centers.txt', "w") as f:
    f.write(str(centers))