import pyspark
import json
import subprocess

spark = (
    pyspark.sql.SparkSession
    .builder
    .appName("Python Spark SQL aggregation")
    .enableHiveSupport()
    .getOrCreate()
)

with open("./CommandLineArguments.json") as f:
    json_as_python_dict = json.load(f)

usage_df = spark.read.csv("./data/full_clean_usage.csv",
                          header=True, inferSchema=True)
churn_df = spark.read.csv(json_as_python_dict.get("path_input_churn"),
                          header=True, inferSchema=True)

usage_df.createOrReplaceTempView("customer_usage")

date_columns = ["year", "month"]
id_columns = ["user_account_id"]
binary_columns = [
    "user_intake",
    "user_has_outgoing_calls", "user_has_outgoing_sms",
    "user_use_gprs", "user_does_reload"
]

categorical_columns = date_columns + binary_columns + id_columns
continuous_columns = [c for c in usage_df.columns
                      if c not in categorical_columns]

sql_expressions_avg = ["AVG({0}) AS {0}".format(c) for c in continuous_columns]


sql_expressions_aggregation = sql_expressions_avg

sql_query_aggregate_by_user_id = """
SELECT user_account_id, {}
FROM customer_usage
GROUP BY user_account_id
""".format("\n , ".join(sql_expressions_aggregation))


aggregate_usage_df = spark.sql(sql_query_aggregate_by_user_id)
aggregate_usage_df.write.csv(json_as_python_dict.get('dir_output'))

subprocess.call("cat ./data/aggregated_usage/* > agg_usage.csv", shell=True)

with open("./data/header__aggregated_customer_usage.txt", "w") as f:
    f.write(",".join(aggregate_usage_df.columns) + "\n")

with open("./data/header__aggregated_customer_usage.txt") as f:
    columns = f.read().rstrip().split(",")

aggregate_usage_without_columns_df = (
    spark.read.csv("./data/aggregated_usage/", inferSchema=True, header=False)
)

default_column_names = aggregate_usage_without_columns_df.columns

aggregate_usage_columns_map = dict(zip(default_column_names, columns))

aggregate_usage_with_columns_df = (
    aggregate_usage_without_columns_df
    .select([pyspark.sql.functions
            .col(c).alias(aggregate_usage_columns_map.get(c, c))
             for c in default_column_names])
)

usage = subprocess.call("cat  "
                        "./data/header_aggregation.txt "
                        "./data/aggregated_usage/* > "
                        "./data/aggregated_customer_usage.csv ",
                        shell=True)

with open("./data/columns_continuous__agg_usage.txt", "w") as f:
    f.write(",".join(continuous_columns) + "\n")

with open("./data/columns_binary__agg_usage.txt", "w") as f:
    f.write(",".join(binary_columns) + "\n")

with open("./data/columns_ids__agg_usage.txt", "w") as f:
    f.write(",".join(id_columns) + "\n")

with open("./data/columns_misc__agg_usage.txt", "w") as f:
    f.write(",".join(id_columns) + "\n")

