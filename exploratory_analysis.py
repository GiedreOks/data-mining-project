import subprocess
import pyspark
import pandas as pd
from pyspark.ml import stat
from pyspark.ml import feature

spark = (
    pyspark.sql.SparkSession
    .builder
    .appName("Elements of exploratory analysis using PySpark")
    .enableHiveSupport()
    .getOrCreate()
)

usage_path = "./data/full_clean_usage.csv"
churn_path = "./data/customer_churn.csv"

subprocess.call("mkdir -p ./data/Exploratory_Analysis", shell=True)

usage_df = spark.read.csv(
    usage_path,
    header=True,
    inferSchema=True)

#Basic statistics
(
    usage_df
    .describe()
    .toPandas()
    .transpose()
    .to_csv("./data/Exploratory_Analysis/Description.csv",
                index=True, header=True)
)


#Calculating correlation
columns_correlation = [
    c for c in usage_df.columns
    if c not in {"user_account_id", "year"}
]

assembler_correlation = feature.VectorAssembler(
    inputCols=columns_correlation,
    outputCol="features_correlation")

def compute_corr(df, columns, method="pearson"):
    assembler = feature.VectorAssembler(
        inputCols=columns,
        outputCol="featuresCorrelation")
    corr_featurized_df = assembler.transform(df)
    corr_df = stat.Correlation.corr(corr_featurized_df, "featuresCorrelation", method)
    corr_matrix = corr_df.first()[0].toArray()
    corr_pddf = pd.DataFrame(corr_matrix, columns=columns, index=columns)
    return corr_pddf

corr_pearson_pddf = compute_corr(usage_df, columns_correlation)

corr_pearson_pddf.to_csv(
    "./data/Exploratory_Analysis/corr_pearson__usage.csv",
    index=True, header=True)
