import pyspark
import json
import subprocess
from pyspark.sql.functions import col

spark = (
    pyspark.sql.SparkSession
    .builder
    .appName("Python Spark SQL cleanup")
    .enableHiveSupport()
    .getOrCreate()
)

with open("./CommandLineArguments.json") as f:
    json_as_python_dict = json.load(f)

#Querying columns from the original usage dataset
usage_df = spark.read.csv(json_as_python_dict.get("path_input_usage"),
                          header=True, inferSchema=True)
usage_df.createOrReplaceTempView("customer_usage")

id_columns = ["user_account_id"]
binary_columns = [
    "user_intake",
    "user_has_outgoing_calls", "user_has_outgoing_sms",
    "user_use_gprs", "user_does_reload"
]

date_columns = ["year", "month"]

id_and_to_be_cleaned = ["user_account_id", "user_lifetime",
          "user_no_outgoing_activity_in_days",
          "reloads_inactive_days", "calls_outgoing_inactive_days",
          "sms_outgoing_inactive_days",
          "gprs_inactive_days"]

#Not taking the binary columns and columns to be cleaned
unecessary_columns = binary_columns + id_and_to_be_cleaned
continuous_columns = \
    [c for c in usage_df.columns if c not in unecessary_columns]

usage_query = """
SELECT user_account_id, {}
FROM customer_usage
""".format("\n , ".join(continuous_columns))

usage_cols = spark.sql(usage_query)
usage_cols.write.csv("./data/original_usage")
subprocess.call("cat ./data/original_usage/* > "
                "./data/min_usage.csv", shell=True)

#Saving usage without columns
min_usage_without_columns_df = (
    spark.read.csv("./data/min_usage.csv", inferSchema=True, header=False)
)

with open("./data/header_original_customer_usage.txt", "w") as f:
    f.write(",".join(usage_cols.columns) + "\n")

with open("./data/header_original_customer_usage.txt") as f:
    columns = f.read().rstrip().split(",")

default_column_names = min_usage_without_columns_df.columns

aggregate_usage_columns_map = dict(zip(default_column_names, columns))

aggregate_usage_with_columns_df = (
    min_usage_without_columns_df
    .select([pyspark.sql.functions.col(c).
            alias(aggregate_usage_columns_map.get(c, c))
             for c in default_column_names])
)

#Concatenating headers and stripped usage data
usage = subprocess.call("cat  ./data/header_original_customer_usage.txt "
                        "./data/original_usage/* > "
                        "./data/min_customer_usage_w_header.csv",
                        shell=True)

#Cleaning up selected columns
sql_cleanup = """
    SELECT user_account_id, year, month, 
    CASE 
        WHEN user_lifetime > 15000 THEN 1 
        ELSE user_lifetime/30
    END
    AS user_lifetime_clean,
    
    CASE 
        WHEN user_no_outgoing_activity_in_days > 1200 THEN 0 
        ELSE user_no_outgoing_activity_in_days      
    END
    AS no_activity_clean,
    
    CASE 
        WHEN reloads_inactive_days > 1200 THEN 0 
        ELSE reloads_inactive_days      
    END
    AS reloads_inactive_days_clean,
    
    CASE 
        WHEN calls_outgoing_inactive_days > 1200 THEN 0 
        ELSE calls_outgoing_inactive_days      
    END
    AS calls_outgoing_inactive_days_clean,
    
    CASE 
        WHEN sms_outgoing_inactive_days > 1200 THEN 0 
        ELSE sms_outgoing_inactive_days      
    END
    AS sms_outgoing_inactive_days_clean,
        
    CASE 
        WHEN gprs_inactive_days > 1200 THEN 0 
        ELSE gprs_inactive_days      
    END
    AS gprs_inactive_days_clean
    
    FROM customer_usage
"""

#Cleaned columns headers
header = ["user_account_id", "year", "month", "user_lifetime",
          "user_no_outgoing_activity_in_days",
          "reloads_inactive_days", "calls_outgoing_inactive_days",
          "sms_outgoing_inactive_days",
          "gprs_inactive_days"]

with open("./data/header_cleaned_cols.txt", "w") as f:
    f.write(",".join(header) + "\n")

with open("./data/header_cleaned_cols.txt") as f:
    columns = f.read().rstrip().split(", ")

filtered_usage_df = spark.sql(sql_cleanup)
filtered_usage_df.write.csv('./data/filtered_usage')

subprocess.call("cat ./data/header_cleaned_cols.txt "
                "./data/filtered_usage/* > ./data/clean_usage.csv",
                shell=True)

#Saving cleaned columns
clean_usage_df = spark.read.csv("./data/clean_usage.csv", header=True,
                                inferSchema=True)

clean_usage_df.createOrReplaceTempView("clean_usage")
min_usage_with_columns = spark.read.csv(
    "./data/min_customer_usage_w_header.csv", inferSchema=True, header=True)
min_usage_with_columns.createOrReplaceTempView("customer_usage_min")

#Joining cleaned columns and stripped usage with columns tables
sql_clean_query = """
SELECT 
clean_usage.user_account_id
, clean_usage.year
, clean_usage.month
, clean_usage.user_lifetime
, clean_usage.user_no_outgoing_activity_in_days
, clean_usage.reloads_inactive_days
, clean_usage.calls_outgoing_inactive_days
, clean_usage.sms_outgoing_inactive_days
, clean_usage.gprs_inactive_days
, customer_usage_min.user_account_balance_last
, customer_usage_min.user_spendings
, customer_usage_min.reloads_count
, customer_usage_min.reloads_sum
, customer_usage_min.calls_outgoing_count
, customer_usage_min.calls_outgoing_spendings
, customer_usage_min.calls_outgoing_duration
, customer_usage_min.calls_outgoing_spendings_max
, customer_usage_min.calls_outgoing_duration_max
, customer_usage_min.calls_outgoing_to_onnet_count
, customer_usage_min.calls_outgoing_to_onnet_spendings
, customer_usage_min.calls_outgoing_to_offnet_count
, customer_usage_min.calls_outgoing_to_offnet_spendings
, customer_usage_min.calls_outgoing_to_offnet_duration
, customer_usage_min.calls_outgoing_to_abroad_count
, customer_usage_min.calls_outgoing_to_abroad_spendings
, customer_usage_min.calls_outgoing_to_abroad_duration
, customer_usage_min.sms_outgoing_count
, customer_usage_min.sms_outgoing_spendings
, customer_usage_min.sms_outgoing_spendings_max
, customer_usage_min.sms_outgoing_to_onnet_count
, customer_usage_min.sms_outgoing_to_onnet_spendings
, customer_usage_min.sms_outgoing_to_offnet_count
, customer_usage_min.sms_outgoing_to_abroad_count
, customer_usage_min.sms_outgoing_to_abroad_spendings
, customer_usage_min.sms_incoming_count
, customer_usage_min.sms_incoming_spendings
, customer_usage_min.sms_incoming_from_abroad_count
, customer_usage_min.sms_incoming_from_abroad_spendings
, customer_usage_min.gprs_usage
, customer_usage_min.gprs_spendings
, customer_usage_min.last_100_reloads_count
, customer_usage_min.last_100_reloads_sum
, customer_usage_min.last_100_calls_outgoing_duration
, customer_usage_min.last_100_calls_outgoing_to_onnet_duration
, customer_usage_min.last_100_calls_outgoing_to_abroad_duration
, customer_usage_min.last_100_sms_outgoing_count
, customer_usage_min.last_100_sms_outgoing_to_onnet_count
, customer_usage_min.last_100_sms_outgoing_to_offnet_count
, customer_usage_min.last_100_sms_outgoing_to_abroad_count
, customer_usage_min.last_100_gprs_usage
FROM clean_usage
  INNER JOIN customer_usage_min
    ON clean_usage.user_account_id = customer_usage_min.user_account_id AND
    clean_usage.year = customer_usage_min.year AND
    clean_usage.month = customer_usage_min.month 
"""

total_usage_df = spark.sql(sql_clean_query)
total_usage_df.write.csv('./data/total_usage')

#Preparing headers for the final CSV file
removed_columns = ["calls_outgoing_to_onnet_inactive_days",
                   "calls_outgoing_to_offnet_inactive_days",
                   "calls_outgoing_to_abroad_inactive_days",
                   "sms_outgoing_to_onnet_inactive_days",
                   "sms_outgoing_to_offnet_inactive_days",
                   "sms_outgoing_to_abroad_inactive_days",
                   "calls_outgoing_to_onnet_duration",
                   "sms_outgoing_to_offnet_spendings",
                   "gprs_session_count",
                   "last_100_calls_outgoing_to_offnet_duration"]

redundant_final_usage_headers = date_columns + id_columns + \
                                id_and_to_be_cleaned + binary_columns +\
                                removed_columns
final_usage_cols = header + [c for c in
                             usage_df.columns
                             if c not in redundant_final_usage_headers]


with open("./data/header_final_customer_usage.txt", "w") as f:
    f.write(",".join(final_usage_cols) + "\n")

with open("./data/header_final_customer_usage.txt") as f:
    final_columns = f.read().rstrip().split(",")

clean_cols_to_aggregate = ["user_account_id", "user_lifetime",
          "user_no_outgoing_activity_in_days",
          "reloads_inactive_days", "calls_outgoing_inactive_days",
          "sms_outgoing_inactive_days",
          "gprs_inactive_days"]

headers_to_be_aggregated = clean_cols_to_aggregate + [c for c in
                             usage_df.columns
                             if c not in redundant_final_usage_headers]

with open("./data/header_aggregation.txt", "w") as f:
    f.write(",".join(headers_to_be_aggregated) + "\n")

with open("./data/header_aggregation.txt") as f:
    headers_to_be_aggregated = f.read().rstrip().split(",")


#Saving the result as full_clean_usage.csv in data repository
subprocess.call("cat ./data/header_final_customer_usage.txt "
                "./data/total_usage/* > ./data/full_clean_usage.csv",
                shell=True)